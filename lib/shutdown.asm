;; This file is part of asmc, a bootstrapping OS with minimal seed
;; Copyright (C) 2018 Giovanni Mascellani <gio@debian.org>
;; https://gitlab.com/giomasce/asmc

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

  section .text

qemu_shutdown:
  ;; mov edx, 0x501                ; QEMU debug shutdown (requires "-device isa-debug-exit")
  ;; out dx, al
  mov edx, 0x604                ; QEMU ACPI shutdown (see https://wiki.osdev.org/Shutdown)
  mov eax, 0x2000
  out dx, ax
  ret

shutdown:
  call qemu_shutdown
  hlt
  jmp shutdown
