/* This file is part of asmc, a bootstrapping OS with minimal seed
   Copyright (C) 2019 Giovanni Mascellani <gio@debian.org>
   https://gitlab.com/giomasce/asmc

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <asmc_api.h>

int main(int argc, char *argv[]) {
    printf("Hello world from continue.c!\n");
    download_file("http://10.0.2.2:8080/continue2.c", "/ram/continue2.c");
    void *cont2_buf;
    int (*cont2_symb)(int, char*[]) = compile_file("/ram/continue2.c", "_start", &cont2_buf);
    char *cont2_argv[] = {"_main"};
    cont2_symb(sizeof(cont2_argv) / sizeof(cont2_argv[0]), cont2_argv);
    free(cont2_buf);
    return 0;
}
