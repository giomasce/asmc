
  ;; Inspired to https://wiki.osdev.org/Real_Mode

  org 0x500

  bits 32
  ;; Interrupts should already be disabled, but who knows...
  cli

  ;; Load 16 bit GDT prepared by C code
  lgdt [real_gdt]
  jmp 0x10:disable_real_mode

  bits 16
disable_real_mode:
  ;; Load 16 bit data segments
  mov ax, 0x18
  mov ds, ax
  mov es, ax
  mov fs, ax
  mov gs, ax
  mov ss, ax

  ;; Disable protected mode (assume that paging is already disabled)
  mov eax, cr0
  and eax, 0xfffffffe
  mov cr0, eax
  jmp 0x0:real_mode

real_mode:
  ;; Reload segments and stack pointer
  mov ax, 0
  mov ds, ax
  mov es, ax
  mov fs, ax
  mov gs, ax
  mov ss, ax
  mov sp, 0xe000

  ;; Reload IDT
  lidt [real_idt]

  ;; Now load segments for Linux booting
  mov ax, 0x1000
  mov ds, ax
  mov es, ax
  mov fs, ax
  mov gs, ax
  mov ss, ax
  mov sp, 0xe000  

  ;; Boot Linux
  jmp 0x1020:0x0000

  align 4

  ;; IDT data
real_idt:
  dw 0x3ff
  dd 0x00000000

  align 4

real_gdt:
  dw 31
  dd 0x00000800
