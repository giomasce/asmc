#!/bin/bash

mkdir artifacts
mkdir artifacts/tests
mkdir artifacts/mm0
mkdir artifacts/ipxe
mkdir artifacts/single_cream
mkdir artifacts/mm0_c
mkdir artifacts/full

set -e

rm -fr build
sed -i -e 's|const TEST_ALL .|const TEST_ALL 1|' asmg/main.g
sed -i -e 's|const RUN_MM0 .|const RUN_MM0 0|' asmg/main.g
sed -i -e 's|const RUN_TINYCC .|const RUN_TINYCC 0|' asmg/main.g
sed -i -e 's|#define RUN_IPXE .|#define RUN_IPXE 1|' diskfs/run_tcc.c
sed -i -e 's|#define RUN_SINGLE_CREAM .|#define RUN_SINGLE_CREAM 0|' diskfs/run_tcc.c
sed -i -e 's|#define RUN_MM0_C .|#define RUN_MM0_C 0|' diskfs/run_tcc.c
make
cp build/boot_asmg.x86.qcow2 build/boot_asmg_dbg.x86.qcow2 artifacts/tests

rm -fr build
sed -i -e 's|const TEST_ALL .|const TEST_ALL 0|' asmg/main.g
sed -i -e 's|const RUN_MM0 .|const RUN_MM0 1|' asmg/main.g
sed -i -e 's|const RUN_TINYCC .|const RUN_TINYCC 0|' asmg/main.g
sed -i -e 's|#define RUN_IPXE .|#define RUN_IPXE 1|' diskfs/run_tcc.c
sed -i -e 's|#define RUN_SINGLE_CREAM .|#define RUN_SINGLE_CREAM 0|' diskfs/run_tcc.c
sed -i -e 's|#define RUN_MM0_C .|#define RUN_MM0_C 0|' diskfs/run_tcc.c
make
cp build/boot_asmg.x86.qcow2 build/boot_asmg_dbg.x86.qcow2 artifacts/mm0

rm -fr build
sed -i -e 's|const TEST_ALL .|const TEST_ALL 0|' asmg/main.g
sed -i -e 's|const RUN_MM0 .|const RUN_MM0 0|' asmg/main.g
sed -i -e 's|const RUN_TINYCC .|const RUN_TINYCC 1|' asmg/main.g
sed -i -e 's|#define RUN_IPXE .|#define RUN_IPXE 1|' diskfs/run_tcc.c
sed -i -e 's|#define RUN_SINGLE_CREAM .|#define RUN_SINGLE_CREAM 0|' diskfs/run_tcc.c
sed -i -e 's|#define RUN_MM0_C .|#define RUN_MM0_C 0|' diskfs/run_tcc.c
make
cp build/boot_asmg.x86.qcow2 build/boot_asmg_dbg.x86.qcow2 artifacts/ipxe

rm -fr build
sed -i -e 's|const TEST_ALL .|const TEST_ALL 0|' asmg/main.g
sed -i -e 's|const RUN_MM0 .|const RUN_MM0 0|' asmg/main.g
sed -i -e 's|const RUN_TINYCC .|const RUN_TINYCC 1|' asmg/main.g
sed -i -e 's|#define RUN_IPXE .|#define RUN_IPXE 0|' diskfs/run_tcc.c
sed -i -e 's|#define RUN_SINGLE_CREAM .|#define RUN_SINGLE_CREAM 1|' diskfs/run_tcc.c
sed -i -e 's|#define RUN_MM0_C .|#define RUN_MM0_C 0|' diskfs/run_tcc.c
make
cp build/boot_asmg.x86.qcow2 build/boot_asmg_dbg.x86.qcow2 artifacts/single_cream

rm -fr build
sed -i -e 's|const TEST_ALL .|const TEST_ALL 0|' asmg/main.g
sed -i -e 's|const RUN_MM0 .|const RUN_MM0 0|' asmg/main.g
sed -i -e 's|const RUN_TINYCC .|const RUN_TINYCC 1|' asmg/main.g
sed -i -e 's|#define RUN_IPXE .|#define RUN_IPXE 0|' diskfs/run_tcc.c
sed -i -e 's|#define RUN_SINGLE_CREAM .|#define RUN_SINGLE_CREAM 0|' diskfs/run_tcc.c
sed -i -e 's|#define RUN_MM0_C .|#define RUN_MM0_C 1|' diskfs/run_tcc.c
make
cp build/boot_asmg.x86.qcow2 build/boot_asmg_dbg.x86.qcow2 artifacts/mm0_c

rm -fr build
sed -i -e 's|const TEST_ALL .|const TEST_ALL 1|' asmg/main.g
sed -i -e 's|const RUN_MM0 .|const RUN_MM0 1|' asmg/main.g
sed -i -e 's|const RUN_TINYCC .|const RUN_TINYCC 1|' asmg/main.g
sed -i -e 's|#define RUN_IPXE .|#define RUN_IPXE 1|' diskfs/run_tcc.c
sed -i -e 's|#define RUN_SINGLE_CREAM .|#define RUN_SINGLE_CREAM 0|' diskfs/run_tcc.c
sed -i -e 's|#define RUN_MM0_C .|#define RUN_MM0_C 0|' diskfs/run_tcc.c
make
cp build/boot_asmg.x86.qcow2 build/boot_asmg_dbg.x86.qcow2 artifacts/full
