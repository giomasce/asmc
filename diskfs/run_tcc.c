/* This file is part of asmc, a bootstrapping OS with minimal seed
   Copyright (C) 2019 Giovanni Mascellani <gio@debian.org>
   https://gitlab.com/giomasce/asmc

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#include "run_tcc.h"

#include <stdio.h>
#include <stdlib.h>

#ifndef LEVEL
#define LEVEL 0
#endif

#define RUN_IPXE 1
#define RUN_SINGLE_CREAM 0
#define RUN_MM0_C 0

int recursively_compile() {
#if LEVEL == 10
    printf("Let's not get mad, stopping recursion...\n");
    return 0;
#endif

    const char *includes[7];
    includes[0] = ASMC_PREFIX;
    includes[1] = ASMC_PREFIX "/stdlib";
    includes[2] = ASMC_PREFIX "/tinycc-aux";
    includes[3] = ASMC_PREFIX "/tinycc";
    includes[4] = ASMC_PREFIX "/tinycc/softfloat";
    includes[5] = ASMC_PREFIX "/tinycc/softfloat/include";
    includes[6] = ASMC_PREFIX "/tinycc/softfloat/8086";
    const char *files[1];

    printf("Compiling tinycc recursively... (level %d)\n", LEVEL);
#if LEVEL == 1 && RUN_IPXE
#define ADD_TCC_SYMBOLS
    files[0] = ASMC_PREFIX "/run_tcc_ipxe.c";
#elif LEVEL == 1 && RUN_SINGLE_CREAM
#define ADD_TCC_SYMBOLS
    files[0] = ASMC_PREFIX "/run_tcc_sc.c";
#elif LEVEL == 1 && RUN_MM0_C
#define ADD_TCC_SYMBOLS
    files[0] = ASMC_PREFIX "/run_tcc_mm0_c.c";
#else
    files[0] = ASMC_PREFIX "/run_tcc.c";
#endif

    TCCState *state = tcc_new();
    char buf[1024];
    sprintf(buf, "%d", __get_handles());
    tcc_define_symbol(state, "__HANDLES", buf);
    sprintf(buf, "%d", LEVEL+1);
    tcc_define_symbol(state, "LEVEL", buf);

#ifdef ADD_TCC_SYMBOLS
    tcc_add_symbol(state, "tcc_new", tcc_new);
    tcc_add_symbol(state, "tcc_delete", tcc_delete);
    tcc_add_symbol(state, "tcc_set_lib_path", tcc_set_lib_path);
    tcc_add_symbol(state, "tcc_set_error_func", tcc_set_error_func);
    tcc_add_symbol(state, "tcc_set_options", tcc_set_options);
    tcc_add_symbol(state, "tcc_add_include_path", tcc_add_include_path);
    tcc_add_symbol(state, "tcc_add_sysinclude_path", tcc_add_sysinclude_path);
    tcc_add_symbol(state, "tcc_define_symbol", tcc_define_symbol);
    tcc_add_symbol(state, "tcc_undefine_symbol", tcc_undefine_symbol);
    tcc_add_symbol(state, "tcc_add_file", tcc_add_file);
    tcc_add_symbol(state, "tcc_compile_string", tcc_compile_string);
    tcc_add_symbol(state, "tcc_set_output_type", tcc_set_output_type);
    tcc_add_symbol(state, "tcc_add_library_path", tcc_add_library_path);
    tcc_add_symbol(state, "tcc_add_library", tcc_add_library);
    tcc_add_symbol(state, "tcc_add_symbol", tcc_add_symbol);
    tcc_add_symbol(state, "tcc_output_file", tcc_output_file);
    tcc_add_symbol(state, "tcc_run", tcc_run);
    tcc_add_symbol(state, "tcc_relocate", tcc_relocate);
    tcc_add_symbol(state, "tcc_get_symbol", tcc_get_symbol);
#endif

    if (run_tinycc(state, sizeof(files) / sizeof(files[0]), files,
                   sizeof(includes) / sizeof(includes[0]), includes)) {
        "tinycc invocation failed...\n";
        return 1;
    }

    int (*start)() = tcc_get_symbol(state, "_start");
    if (!start) {
        printf("tcc_get_symbol() failed\n");
        return 1;
    }
    printf("Entering tinycc...\n");
    int ret = start();
    printf("Tinycc returned %d!\n", ret);

    tcc_delete(state);
    return 0;
}

int main(int argc, char *argv[]) {
    if (recursively_compile()) {
        printf("Recursive compilation failed...\n");
        return 1;
    }
    return 0;
}

// Include the actual tinycc compiler
#define USE_SOFTFLOAT 1
#define ONE_SOURCE 1

#include "libtcc.c"
