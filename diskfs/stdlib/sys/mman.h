#ifndef __SYS_MMAN_H
#define __SYS_MMAN_H

#include "asmc_types.h"
#include "errno.h"
#include "stdio.h"
#include "unistd.h"

#define PROT_NONE 0
#define PROT_EXEC (1 << 0)
#define PROT_READ (1 << 1)
#define PROT_WRITE (1 << 2)

#define MAP_FIXED (1 << 0)
#define MAP_PRIVATE (1 << 1)
#define MAP_SHARED (1 << 2)
#define MAP_FILE (1 << 3)

#define MAP_FAILED NULL

int mprotect(void *addr, size_t len, int prot) {
    if (prot == PROT_READ | PROT_WRITE | PROT_EXEC) {
        // There is no protection, so all memory is automatically rwx
        return 0;
    } else {
        errno = ENOTIMPL;
        return -1;
    }
}

void *mmap(void *addr, size_t len, int prot, int flags, int fildes, off_t off) {
    // Ignore MAP_FILE
    flags &= ~MAP_FILE;
    if (addr != 0 || prot != PROT_READ || flags != MAP_PRIVATE) {
        errno = ENOTIMPL;
        return MAP_FAILED;
    }
    void *ret = malloc(len);
    if (len == 0) {
        return ret;
    }
    if (ret == NULL) {
        errno = ENOMEM;
        return MAP_FAILED;
    }
    char *pos = (char*) ret;
    off_t cur_pos = lseek(fildes, 0, SEEK_CUR);
    lseek(fildes, off, SEEK_SET);
    do {
        ssize_t res = read(fildes, pos, 1);
        if (!res) {
            *pos = '\0';
        }
        pos++;
    } while (--len);
    lseek(fildes, cur_pos, SEEK_SET);
    return ret;
}

int munmap(void *addr, size_t len) {
    free(addr);
    return 0;
}

#endif
