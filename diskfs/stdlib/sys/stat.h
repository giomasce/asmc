#ifndef __SYS_STAT_H
#define __SYS_STAT_H

#include "asmc.h"
#include "errno.h"
#include "sys/types.h"
#include <time.h>
#include "unistd.h"

#define S_IRWXU 0700
#define S_IRUSR 0400
#define S_IWUSR 0200
#define S_IXUSR 0100
#define S_IRWXG 0070
#define S_IRGRP 0040
#define S_IWGRP 0020
#define S_IXGRP 0010
#define S_IRWXO 0007
#define S_IROTH 0004
#define S_IWOTH 0002
#define S_IXOTH 0001
#define S_ISUID 04000
#define S_ISGID 02000
#define S_ISVTX 01000

struct stat {
    dev_t st_dev;
    ino_t st_ino;
    mode_t st_mode;
    nlink_t st_nlink;
    uid_t st_uid;
    gid_t st_gid;
    dev_t st_rdev;
    off_t st_size;
    struct timespec st_atim;
    struct timespec st_mtim;
    struct timespec st_ctim;
    blksize_t st_blksize;
    blkcnt_t st_blocks;
};

int fstat(int fildes, struct stat *buf) {
    buf->st_dev = 0;
    buf->st_ino = 0;
    buf->st_mode = 0;
    buf->st_nlink = 1;
    buf->st_uid = 0;
    buf->st_gid = 0;
    buf->st_rdev = 0;
    off_t cur_pos = lseek(fildes, 0, SEEK_CUR);
    buf->st_size = lseek(fildes, 0, SEEK_END);
    lseek(fildes, cur_pos, SEEK_SET);
    buf->st_atim.tv_sec = 0;
    buf->st_atim.tv_nsec = 0;
    buf->st_mtim.tv_sec = 0;
    buf->st_mtim.tv_nsec = 0;
    buf->st_ctim.tv_sec = 0;
    buf->st_ctim.tv_nsec = 0;
    buf->st_blksize = 512;
    buf->st_blocks = (buf->st_size+511)/512;
    return 0;
}

// STUB
int chmod(const char *path, mode_t mode) {
    __unimplemented();
    errno = ENOTIMPL;
    return -1;
}

#endif
