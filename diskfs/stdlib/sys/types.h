#ifndef __SYS_TYPES_H
#define __SYS_TYPES_H

#include "asmc_types.h"

typedef unsigned int mode_t;
typedef int pid_t;
typedef unsigned int uid_t;
typedef unsigned int gid_t;
typedef unsigned int dev_t;
typedef unsigned int ino_t;
typedef unsigned int nlink_t;
typedef int blksize_t;
typedef int blkcnt_t;

#endif
