/* This file is part of asmc, a bootstrapping OS with minimal seed
   Copyright (C) 2019 Giovanni Mascellani <gio@debian.org>
   https://gitlab.com/giomasce/asmc

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#ifndef __IPXE_HANDOVER_H
#define __IPXE_HANDOVER_H

typedef struct {
    char *name;
    char *data;
    int len;
} table_sect;

typedef struct ipxe_list_elem {
    struct ipxe_list_elem *next;
    void *msg;
} ipxe_list_elem;

typedef struct ipxe_list {
    ipxe_list_elem *head;
    ipxe_list_elem *tail;
} ipxe_list;

typedef struct {
    int sects_num;
    table_sect *sects;
    ipxe_list to_ipxe;
    ipxe_list from_ipxe;
    void (*coro_yield)();
    void *(*malloc)(size_t size);
    void (*free)(void *ptr);
    void *(*realloc)(void *ptr, size_t size);
} ipxe_handover;

static void ipxe_list_reset(ipxe_list *list) {
    list->head = 0;
    list->tail = 0;
}

static void ipxe_list_push(ipxe_handover *ih, ipxe_list *list, void *msg) {
    ipxe_list_elem *e = ih->malloc(sizeof(ipxe_list_elem));
    e->next = 0;
    e->msg = msg;
    if (!list->head) {
        // Empty list, this is the first and last item
        list->head = e;
        list->tail = e;
    } else {
        // Pushing after tail
        list->tail->next = e;
        list->tail = e;
    }
}

static void *ipxe_list_pop(ipxe_handover *ih, ipxe_list *list) {
    ipxe_list_elem *e = list->head;
    if (!e) {
        // Empty list, return zero
        return 0;
    } else {
        // Return head element
        void *msg = e->msg;
        list->head = e->next;
        if (!list->head) {
            list->tail = 0;
        }
        ih->free(e);
        return msg;
    }
}

#endif
