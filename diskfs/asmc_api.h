/* This file is part of asmc, a bootstrapping OS with minimal seed
   Copyright (C) 2019 Giovanni Mascellani <gio@debian.org>
   https://gitlab.com/giomasce/asmc

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#include "asmc.h"
#include "ipxe_asmc_structs.h"

#include "sha-2/sha-256.h"

void download_file(const char *url, const char *dest_file);
void *compile_file(const char *code, const char *symbol, void **buffer);
void *read_whole_file(FILE *fin, size_t *size);
void compute_file_sha256(const char *filename, uint8_t hash[32]);
